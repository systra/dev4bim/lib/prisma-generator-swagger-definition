"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.genModelArray = exports.genPartialModelArray = exports.genModel = exports.genPartialModel = exports.genKeyModel = exports.genSchema = void 0;
const _ = __importStar(require("lodash"));
const properties_1 = require("./properties");
const idsFields = [
    'id',
    'pk',
    '_id'
];
const documentationContainsKey = (key, documentation) => {
    if (!documentation)
        return false;
    const keys = documentation.split('.')[0].toUpperCase();
    return keys.includes(key.toUpperCase());
};
const fieldDescriptionReducer = (acc, curr) => {
    acc[curr.name] = (0, properties_1.getSchemaProperty)(curr);
    return acc;
};
const fieldsTagWithKey = (key, fields) => {
    return fields
        .filter(f => !idsFields.includes(f.name))
        .filter((f) => documentationContainsKey(key, f.documentation))
        .reduce(fieldDescriptionReducer, {});
};
const Allfields = (fields) => {
    return fields
        .filter(f => !idsFields.includes(f.name))
        .reduce(fieldDescriptionReducer, {});
};
const genSchema = (model, keys) => {
    const keyGenModels = keys.flatMap(key => (0, exports.genKeyModel)(key, model)).reduce((acc, curr) => {
        for (const key in curr) {
            acc[key] = curr[key];
        }
        return acc;
    }, {});
    return {
        components: {
            schemas: {
                ...(0, exports.genPartialModel)(model),
                ...keyGenModels,
                ...(0, exports.genModel)(model),
                ...(0, exports.genPartialModelArray)(model),
                ...(0, exports.genModelArray)(model)
            }
        }
    };
};
exports.genSchema = genSchema;
const genKeyModel = (key, { fields, name }) => {
    const componentName = `${_.upperFirst(key)}${name}`;
    const properties = fieldsTagWithKey(key, fields);
    if (Object.keys(properties).length == 0)
        return null;
    const schema = {};
    schema[componentName] = {
        type: 'object',
        required: Object.keys(properties),
        properties
    };
    return schema;
};
exports.genKeyModel = genKeyModel;
const genPartialModel = ({ fields, name }) => {
    const componentName = `Partial${name}`;
    const properties = Allfields(fields);
    const schema = {};
    schema[componentName] = {
        type: 'object',
        properties
    };
    return schema;
};
exports.genPartialModel = genPartialModel;
const genModel = ({ fields, name, documentation }) => {
    const componentName = name;
    const required = fields.filter(f => !idsFields
        .includes(f.name)).filter(f => f.isRequired && f.relationName == undefined)
        .map(f => f.name);
    const schema = {};
    schema[componentName] = {
        allOf: [
            {
                '$ref': '#/components/schemas/DbObject'
            },
            {
                '$ref': `#/components/schemas/Partial${name}`
            },
            {
                type: 'object',
                description: documentation,
                required
            }
        ]
    };
    return schema;
};
exports.genModel = genModel;
const genPartialModelArray = ({ fields, name }) => {
    const componentName = `ArrayOfPartial${name}`;
    const schema = {};
    schema[componentName] = {
        type: 'array',
        items: {
            '$ref': `#/components/schemas/Partial${name}`
        }
    };
    return schema;
};
exports.genPartialModelArray = genPartialModelArray;
const genModelArray = ({ fields, name }) => {
    const componentName = `ArrayOf${name}`;
    const schema = {};
    schema[componentName] = {
        type: 'array',
        items: {
            '$ref': `#/components/schemas/${name}`
        }
    };
    return schema;
};
exports.genModelArray = genModelArray;
//# sourceMappingURL=genModels.js.map