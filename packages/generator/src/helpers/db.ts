import { ConnectorType } from "@prisma/generator-helper"


export const genDbType = (provider: ConnectorType, dbIdsOption : string) => {
    let DbObject;
    switch (provider) {
        case 'postgresql':
            const dbIds = dbIdsOption ? dbIdsOption.split(' ') : ["pk", "id"]
            DbObject = postgresqlObject(dbIds);
            break;
        default:
            break;
    }

    return {
        components: {
            schemas: {
                DbObject
            }
        }
    }
}

const postgresqlObject = (ids: Array<string>) => ({
    type: 'object',
    required: ids,
    properties: ids.reduce((acc: Record<string, { type : string}>, curr) =>{
        acc[curr] = { type: curr == 'pk' ? 'integer' : 'string'}
        return acc;
    }, {})
})