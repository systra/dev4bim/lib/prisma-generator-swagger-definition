"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const generator_helper_1 = require("@prisma/generator-helper");
const path_1 = __importDefault(require("path"));
const constants_1 = require("./constants");
const genModels_1 = require("./helpers/genModels");
const writeFileSafely_1 = require("./utils/writeFileSafely");
const db_1 = require("./helpers/db");
const { version } = require('../package.json');
(0, generator_helper_1.generatorHandler)({
    onManifest() {
        console.info(`${constants_1.GENERATOR_NAME}:Registered`);
        return {
            version,
            defaultOutput: '../generated',
            prettyName: constants_1.GENERATOR_NAME,
        };
    },
    onGenerate: async (options) => {
        var _a;
        const dbType = (0, db_1.genDbType)(options.datasources[0].provider, options.generator.config.dbIds);
        const writeLocation = path_1.default.join((_a = options.generator.output) === null || _a === void 0 ? void 0 : _a.value, `DbObject.yaml`);
        await (0, writeFileSafely_1.writeFileSafely)(writeLocation, dbType);
        const keys = options.generator.config.keys ? options.generator.config.keys.split(' ') : [];
        options.dmmf.datamodel.models.forEach(async (modelInfo) => {
            var _a;
            const yamlModel = (0, genModels_1.genSchema)(modelInfo, keys);
            const writeLocation = path_1.default.join((_a = options.generator.output) === null || _a === void 0 ? void 0 : _a.value, `${modelInfo.name}.yaml`);
            await (0, writeFileSafely_1.writeFileSafely)(writeLocation, yamlModel);
        });
    },
});
//# sourceMappingURL=generator.js.map