import { GeneratorOptions, generatorHandler } from '@prisma/generator-helper';
import path from 'path';
import { GENERATOR_NAME } from './constants'
import { genSchema } from './helpers/genModels'
import { writeFileSafely } from './utils/writeFileSafely'
import { genDbType } from './helpers/db'

const { version } = require('../package.json')
generatorHandler({
  onManifest() {
    console.info(`${GENERATOR_NAME}:Registered`)
    return {
      version,
      defaultOutput: '../generated',
      prettyName: GENERATOR_NAME,
    }
  },
  onGenerate: async (options: GeneratorOptions) => {
    const dbType = genDbType(options.datasources[0].provider, options.generator.config.dbIds as string);
    const writeLocation = path.join(
      options.generator.output?.value!,
      `DbObject.yaml`,
    )

    await writeFileSafely(writeLocation, dbType)
    const keys = options.generator.config.keys ? (options.generator.config.keys as string).split(' ') : [];
 
    options.dmmf.datamodel.models.forEach(async (modelInfo) => {
      const yamlModel = genSchema(modelInfo, keys)

      const writeLocation = path.join(
        options.generator.output?.value!,
        `${modelInfo.name}.yaml`,
      )

      await writeFileSafely(writeLocation, yamlModel)
    })
  },
})
