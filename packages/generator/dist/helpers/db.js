"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.genDbType = void 0;
const genDbType = (provider, dbIdsOption) => {
    let DbObject;
    switch (provider) {
        case 'postgresql':
            const dbIds = dbIdsOption ? dbIdsOption.split(' ') : ["pk", "id"];
            DbObject = postgresqlObject(dbIds);
            break;
        default:
            break;
    }
    return {
        components: {
            schemas: {
                DbObject
            }
        }
    };
};
exports.genDbType = genDbType;
const postgresqlObject = (ids) => ({
    type: 'object',
    required: ids,
    properties: ids.reduce((acc, curr) => {
        acc[curr] = { type: curr == 'pk' ? 'integer' : 'string' };
        return acc;
    }, {})
});
//# sourceMappingURL=db.js.map