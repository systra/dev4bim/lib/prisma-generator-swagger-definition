"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toCamelCase = exports.assertNever = exports.isDefined = exports.isEnumType = exports.isScalarType = void 0;
function isScalarType(field) {
    return field['kind'] === 'scalar';
}
exports.isScalarType = isScalarType;
function isEnumType(field) {
    return field['kind'] === 'enum';
}
exports.isEnumType = isEnumType;
function isDefined(value) {
    return value !== undefined && value !== null;
}
exports.isDefined = isDefined;
function assertNever(value) {
    throw new Error(`Unhandled discriminated union member: ${JSON.stringify(value)}`);
}
exports.assertNever = assertNever;
function toCamelCase(name) {
    return name.substring(0, 1).toLowerCase() + name.substring(1);
}
exports.toCamelCase = toCamelCase;
//# sourceMappingURL=helpers.js.map