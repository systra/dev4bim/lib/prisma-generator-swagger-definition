import prettier from 'prettier'

export const formatFile = (content: string): Promise<string> => {
  return new Promise((res, rej) =>
    prettier.resolveConfig(process.cwd()).then((options) => {
      if (!options) {
        res(content) // no prettier config was found, no need to format
      }

      try {
        const formatted = prettier.format(content, {
          ...options,
          parser: 'yaml',
        })
        
        res(formatted)
      } catch (error) {
        console.log(error);
        
        rej(error)
      }
    })
  )
}
