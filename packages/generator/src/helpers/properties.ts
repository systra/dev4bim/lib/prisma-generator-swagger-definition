import { DMMF } from '@prisma/generator-helper'
import {
    assertNever,
    isEnumType,
    isScalarType,
    PrismaPrimitive,
} from './helpers'


function getSchemaScalar(
    fieldType: PrismaPrimitive,
) {
    switch (fieldType) {
        case 'Int':
        case 'BigInt':
            return 'integer'
        case 'DateTime':
        case 'Bytes':
        case 'String':
            return 'string'
        case 'Float':
        case 'Decimal':
            return 'number'
        case 'Json':
            return 'object'
        case 'Boolean':
            return 'boolean'
        default:
            assertNever(fieldType)
    }
}

function getSchemaType(field: DMMF.Field) {

    const scalarFieldType =
        isScalarType(field) && !field.isList
            ? getSchemaScalar(field.type)
            : field.isList
                ? 'array'
                : isEnumType(field)
                    ? 'string'
                    : 'object'

    const result: any = {
        type : scalarFieldType,
    }
    if (field.type == 'DateTime') {
        result.format = 'date-time'
    }
    if (!field.isRequired) {
        result.nullable = true;
    }
    if (scalarFieldType == 'array') {
        if (field.relationName) {
            result.nullable = true;
            result.items = { '$ref': `#/components/schemas/${field.type}`}
        }else{
            const fieldType = getSchemaScalar(field.type as PrismaPrimitive)
            result.items = {type :fieldType}
        }
    }
    return result

}

export function getSchemaProperty(field: DMMF.Field) {
    const schemaType = getSchemaType(field);
    if (schemaType.type == 'object') {
        return { '$ref': `#/components/schemas/${field.type}` }
    }
    else {
        const description = field.documentation ?
            field.documentation.includes('.') ?
                field.documentation.substring(field.documentation.indexOf('.') + 1)
                : field.documentation
            : undefined
        return {
            ...schemaType,
            description
        }
    }

}