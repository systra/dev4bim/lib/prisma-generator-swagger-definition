"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSchemaProperty = void 0;
const helpers_1 = require("./helpers");
function getSchemaScalar(fieldType) {
    switch (fieldType) {
        case 'Int':
        case 'BigInt':
            return 'integer';
        case 'DateTime':
        case 'Bytes':
        case 'String':
            return 'string';
        case 'Float':
        case 'Decimal':
            return 'number';
        case 'Json':
            return 'object';
        case 'Boolean':
            return 'boolean';
        default:
            (0, helpers_1.assertNever)(fieldType);
    }
}
function getSchemaType(field) {
    const scalarFieldType = (0, helpers_1.isScalarType)(field) && !field.isList
        ? getSchemaScalar(field.type)
        : field.isList
            ? 'array'
            : (0, helpers_1.isEnumType)(field)
                ? 'string'
                : 'object';
    const result = {
        type: scalarFieldType,
    };
    if (field.type == 'DateTime') {
        result.format = 'date-time';
    }
    if (!field.isRequired) {
        result.nullable = true;
    }
    if (scalarFieldType == 'array') {
        if (field.relationName) {
            result.nullable = true;
            result.items = { '$ref': `#/components/schemas/${field.type}` };
        }
        else {
            const fieldType = getSchemaScalar(field.type);
            result.items = { type: fieldType };
        }
    }
    return result;
}
function getSchemaProperty(field) {
    const schemaType = getSchemaType(field);
    if (schemaType.type == 'object') {
        return { '$ref': `#/components/schemas/${field.type}` };
    }
    else {
        const description = field.documentation ?
            field.documentation.includes('.') ?
                field.documentation.substring(field.documentation.indexOf('.') + 1)
                : field.documentation
            : undefined;
        return {
            ...schemaType,
            description
        };
    }
}
exports.getSchemaProperty = getSchemaProperty;
//# sourceMappingURL=properties.js.map