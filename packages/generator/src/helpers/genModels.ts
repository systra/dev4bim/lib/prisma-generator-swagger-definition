import { DMMF } from '@prisma/generator-helper'
import * as _ from 'lodash'
import { getSchemaProperty } from './properties';

const idsFields = [
  'id',
  'pk',
  '_id'
]

const documentationContainsKey = (key: string, documentation: string | undefined) => {
  if (!documentation)
    return false;

  const keys = documentation.split('.')[0].toUpperCase();
  return keys.includes(key.toUpperCase())
}

const fieldDescriptionReducer = (acc: any, curr: DMMF.Field) => {
  acc[curr.name] = getSchemaProperty(curr)
  return acc
}

const fieldsTagWithKey = (key: string, fields: DMMF.Model['fields']) => {
  return fields
    .filter(f => !idsFields.includes(f.name))
    .filter((f: DMMF.Field) => documentationContainsKey(key, f.documentation))
    .reduce(fieldDescriptionReducer, {})
}

const Allfields = (fields: DMMF.Model['fields']) => {
  return fields
    .filter(f => !idsFields.includes(f.name))
    .reduce(fieldDescriptionReducer, {})
}

export const genSchema = (model: DMMF.Model, keys: Array<string>) => {
  const keyGenModels = keys.flatMap(key => genKeyModel(key, model)).reduce((acc, curr) => {
    for (const key in curr) {
      acc[key] = curr[key]
    }
    return acc;
  }, {})

  return {
    components: {
      schemas: {
        ...genPartialModel(model),
        ...keyGenModels,
        ...genModel(model),
        ...genPartialModelArray(model),
        ...genModelArray(model)
      }
    }
  }
}

export const genKeyModel = (key: string, { fields, name }: DMMF.Model) => {
  const componentName = `${_.upperFirst(key)}${name}`;
  const properties = fieldsTagWithKey(key, fields)
  if (Object.keys(properties).length == 0)
    return null;

  const schema: Record<string, any> = {}
  schema[componentName] = {
    type: 'object',
    required: Object.keys(properties),
    properties
  }

  return schema
}

export const genPartialModel = ({ fields, name }: DMMF.Model) => {
  const componentName = `Partial${name}`;
  const properties = Allfields(fields)

  const schema: Record<string, any> = {}
  schema[componentName] = {
    type: 'object',
    properties
  }

  return schema
}

export const genModel = ({ fields, name, documentation }: DMMF.Model) => {
  const componentName = name;
  const required = fields.filter(f => !idsFields
    .includes(f.name)).filter(f =>
      f.isRequired && f.relationName == undefined)
    .map(f => f.name);
  const schema: Record<string, any> = {}
  schema[componentName] = {
    allOf: [
      {
        '$ref': '#/components/schemas/DbObject'
      },
      {
        '$ref': `#/components/schemas/Partial${name}`
      },
      {
        type: 'object',
        description: documentation,
        required
      }
    ]
  }
  return schema
}

export const genPartialModelArray = ({ fields, name }: DMMF.Model) => {
  const componentName = `ArrayOfPartial${name}`;
  const schema: Record<string, any> = {}
  schema[componentName] = {
    type: 'array',
    items: {
      '$ref': `#/components/schemas/Partial${name}`
    }
  }
  return schema
}

export const genModelArray = ({ fields, name }: DMMF.Model) => {
  const componentName = `ArrayOf${name}`;
  const schema: Record<string, any> = {}
  schema[componentName] = {
    type: 'array',
    items: {
      '$ref': `#/components/schemas/${name}`
    }
  }
  return schema
}